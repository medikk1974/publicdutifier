﻿using DutyFier.Client.Wpf.Generate;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.State
{
    public class DatesSelectionState : IGenerationState
    {
        public UserControl CurrentStateControl { get; set; }
        public GenerateContext Context { get; set; }
        public bool IsBackwardStateAllowed { get; set; }
        public bool IsForwardStateAllowed { get; set; }
        public DatesSelectionState(GenerateContext generateContext)
        {
            Context = generateContext;
            IsBackwardStateAllowed = false;
            IsForwardStateAllowed = true;
            CurrentStateControl = new SelectDatesView(Context);
        }

        public IGenerationState GoBackward()
        {
            return null;
        }

        public IGenerationState GoForward()
        {
            return new DatesExcludingState(Context);
        }
    }
}
