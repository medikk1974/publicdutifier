﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using DutyFier.Core;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Unity;


namespace DutyFier.Client.Wpf.Settings
{
    public class AddPositionViewModel : INotifyPropertyChanged
    {
        private const double DEFAULT_WEIGHT = 1.0f;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public Position  _position;
        public List<DutyType> _dutyTypes;
        private AddPositionModel AddPositionModel { get; set; }
        public AddPositionViewModel(DutyTypeRepository dutyTypeRepository, AddPositionModel addPositionModel)
        {
            _position = new Position();
            AddCommand = new RelayCommands(OnAdd,()=>true);
            DutyTypes = dutyTypeRepository.GetAll().ToList();
            AddPositionModel = addPositionModel;
        }
        public List<DutyType> DutyTypes { get => _dutyTypes; set => _dutyTypes = value; }
        public DutyType SelectedDutyType { get; set; }
        public string Name { get; set; }
        public double Weight { get; set; } = DEFAULT_WEIGHT;
        public bool IsSeniorPosition { get; set; }
        public RelayCommands AddCommand { get; set; }

        private void OnAdd()
        {
            try
            {
                if (!Name.Equals("") && SelectedDutyType != null && Weight > 0)
                {
                    _position = new Position();
                    _position.DutyType = SelectedDutyType;
                    SelectedDutyType = null;
                    OnPropertyChanged(nameof(SelectedDutyType));
                    _position.Name = Name;
                    Name = "";
                    OnPropertyChanged(nameof(Name));
                    _position.Weight = Weight;
                    Weight = DEFAULT_WEIGHT;
                    OnPropertyChanged(nameof(Weight));
                    _position.IsSeniorPosition = IsSeniorPosition;
                    IsSeniorPosition = false;
                    OnPropertyChanged(nameof(IsSeniorPosition));
                    AddPositionModel.AddPositionToDB(_position);
                }
            }
            catch
            {

            }
        }
    }
}
