﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Services
{
    public class ChangeUiServices
    {
        private UIElementCollection _scen;
        public UIElementCollection CarentScen { get { return _scen; } }

        public ChangeUiServices()
        {

        }

        public void SetScen(UIElementCollection uIElement)
        {
            _scen = uIElement;
        }

        public void CangeUserControl(UserControl userControl)
        {
            CarentScen.Clear();
            CarentScen.Add(userControl);
        }
    }
}
