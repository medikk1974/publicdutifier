﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using DutyFier.Core;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Unity;

namespace DutyFier.Client.Wpf.Settings.MainSettings
{
    public class PositionsViewModel : INotifyPropertyChanged
    {
        public SettingsModel SettingsModel { get; set; }
        public PersonRepository PersonReposytory { get; }
        public ObservableCollection<Position> Allpositions { get; set; }
        public Position SelectedPositionsToRemove { get; set; }
        public RelayCommands AddPositionsCommand { get; set; }
        public RelayCommands RemovePositionsCommand { get; set; }

        public RelayCommands AddTypeCommand { get; set; }

        private IUnityContainer _container { get; set; }
        public PositionsViewModel(IUnityContainer container,SettingsModel settingsModel, PersonRepository personRepository)
        {
            SettingsModel = settingsModel;
            _container = container;
            PersonReposytory = personRepository;
            Allpositions = new ObservableCollection<Position>(SettingsModel.GetAllPosition());

            AddTypeCommand = new RelayCommands(addTypeCommand, Can);
            AddPositionsCommand = new RelayCommands(addPositionsCommand, Can);
            RemovePositionsCommand = new RelayCommands(removePositionsCommand, Can);
        }

        private void removePositionsCommand()
        {
            SettingsModel.RemovePosition(SelectedPositionsToRemove);
            Allpositions.Remove(SelectedPositionsToRemove);
        }

        private void addTypeCommand()
        {
            AddTypeView addType = _container.Resolve<AddTypeView>(); /* AddTypeView();*/
            if (addType.ShowDialog() == true)
            {
                addType.Close();
            }
        }

        private void addPositionsCommand()
        {
            AddPositionView addPosition = _container.Resolve<AddPositionView>(); /*new AddPositionView();*/
            if (addPosition.ShowDialog() == true)
            {
                Allpositions = new ObservableCollection<Position>(SettingsModel.GetAllPosition());
                OnPropertyChanged("Allpositions");
                addPosition.Close();
            }
        }

        public bool Can()
        {
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}