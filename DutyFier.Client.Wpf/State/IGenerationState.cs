﻿using System.Windows.Controls;

namespace DutyFier.Client.Wpf.State
{
    public interface IGenerationState
    {
        UserControl CurrentStateControl { get; set; } 
        GenerateContext Context { get; set; }
        bool IsBackwardStateAllowed { get; set; }
        bool IsForwardStateAllowed { get; set; }
        IGenerationState GoForward();
        IGenerationState GoBackward();
    }
}
