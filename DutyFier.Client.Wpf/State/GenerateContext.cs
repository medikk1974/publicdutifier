﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using DutyFier.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Unity;
using DutyFier.Core.Exeptions;
using System.Windows;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.State
{
    public class GenerateContext
    {
        private DutyGenerator dutyGenerate;
        public PositionRepository positionRepository { get; set; }
        private DutyTypeRepository dutyTypeRepository { get; set; }
        public PersonRepository personRepository { get; set; }
        public RelayCommands<UIElementCollection> ForvardComand { get; internal set; }
        public DutyRepository DutyRepository { get; set; } 
        public Dictionary<DutyType, List<DateTime>> DutyTypeDate { get; set; }
        public Dictionary<Person, List<DateTime>> ExludeDates { get; set; }

        public ObservableCollection<Duty> duties;

        private ObservableCollection<DutyRequest> dutyRequests;
        public ObservableCollection<DutyRequest> DutyRequests {
            get
            {
                dutyRequests = DutyRequestsOnTheCalendar();
                return dutyRequests;
            } 
        }


        public GenerateContext(PositionRepository posRep, DutyTypeRepository dutyTypeRep, PersonRepository personRep, DutyRepository dutyRep , PersonDutyFeedbackRepository personDutyFeedbackRepository, DaysOfWeekWeightRepository daysOfWeekWeightRepository)
        {
            positionRepository = posRep;
            dutyTypeRepository = dutyTypeRep;
            personRepository = personRep;
            DutyRepository = dutyRep;

            dutyGenerate = new DutyGenerator(personRepository,
                                             personDutyFeedbackRepository,
                                             DutyRepository,
                                             daysOfWeekWeightRepository);

            ExludeDates = personRepository.GetAll().ToDictionary(x => x, x => new List<DateTime>());

            var dtypes = dutyTypeRepository.GetAll().ToList();

            DutyTypeDate = dtypes.ToDictionary(x => x, x => new List<DateTime>());
        }

        private List<ExcludeDates> ConvertToListExludeDates()
        {
            var resault = new List<ExcludeDates>();
            foreach (var person in ExludeDates.Keys)
            {
                resault.Add(new ExcludeDates(person, ExludeDates[person]));
            }
            return resault;
        }

        public void GeneratorRun()
        {
            try
            {
                duties = new ObservableCollection<Duty>(dutyGenerate.Generate(GetUnitedDutyRequsets(dutyRequests.ToList()), ConvertToListExludeDates(), new List<ChangeOnDateWeigth>()));
                DutyRepository.AddRange(duties);

            }
            catch(PersonAvalibleException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public IEnumerable<Person> GetOnlyAviablePersons(Duty selectedDuty)
        {
            var allPersons = personRepository.GetAll();
            var executors = selectedDuty.Executors;
            foreach(var executor in executors)
            {
                allPersons.Remove(executor.Person);
            }
            return allPersons;
        }

        //TODO : Код працюэ але як )|(опа
        private List<DutyRequest> GetUnitedDutyRequsets(List<DutyRequest> dutyRequests)
        {
            List<DutyRequest> res = new List<DutyRequest>();
            while (dutyRequests.Count > 0)
            {
                res.Add(GetDutyUnitedRequest(dutyRequests, dutyRequests.First().DutyType, dutyRequests.First().Date));
            }
            return res;
        }

        //TODO : DutyRequests - ref old code
        private DutyRequest GetDutyUnitedRequest(List<DutyRequest> dutyRequests, DutyType dutyType, DateTime date)
        {
            var bar = dutyRequests;
            var requestsForDayAndType = dutyRequests.Where(request => request.Date.Equals(date) && request.DutyType.Equals(dutyType)).ToList();

            var positions = requestsForDayAndType.SelectMany(request => request.Positions).ToList();

            requestsForDayAndType.ForEach(request => bar.Remove(request));

            return new DutyRequest(date, positions, dutyType, dutyType.Id);
        }


        public void GeneratorRunWhereNoDutys()
        {
            if (duties == null)
            {
                GeneratorRun();
            }
        }

        private ObservableCollection<DutyRequest> DutyRequestsOnTheCalendar()
        {
            var dutyRequestsOnTheCalendar = new ObservableCollection<DutyRequest>();
            DutyRequest dutyRequest;
            List<Position> positionsDublicateList;
   
            for (int i = 0; i < DutyTypeDate.Keys.Count; i++)
            {
                for (int j = 0; j < DutyTypeDate[DutyTypeDate.Keys.ElementAt(i)].Count; j++)
                {
                    for (int k = 0; k < DutyTypeDate.Keys.ElementAt(i).Positions.Count; k++)
                    {
                        positionsDublicateList = new List<Position>();
                        for (int l = 0; l < DutyTypeDate.Keys.ElementAt(i).Positions[k].DefaultPositionCount; l++)
                        {
                            positionsDublicateList.Add(DutyTypeDate.Keys.ElementAt(i).Positions[k]);
                        }

                        //todo туту щось поламалось
                        if (positionsDublicateList.Count != 0)
                        {
                            dutyRequest = new DutyRequest(DutyTypeDate[DutyTypeDate.Keys.ElementAt(i)][j], positionsDublicateList, DutyTypeDate.Keys.ElementAt(i), DutyTypeDate.Keys.ElementAt(i).Id);
                            dutyRequestsOnTheCalendar.Add(dutyRequest);
                        }


                       
                    }
                }
            }
            return dutyRequestsOnTheCalendar;
        }
        private void ChangeDutyReqest()
        {
            var dutyReqests = DutyRequestsOnTheCalendar();
            
            dutyRequests.Clear();
            
            foreach (var item in dutyReqests)
            {
                dutyRequests.Add(item);
            }
        }

        public void Update()
        {
            foreach (var duty  in duties)
            {
                DutyRepository.Update(duty);
            }
        }
    }
}
