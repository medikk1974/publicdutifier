﻿using DutyFier.Core.Models;
using DutyFier.Core;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Unity;
using System.Windows;

namespace DutyFier.Client.Wpf.Statistics
{
    public class StatisticsViewModel :INotifyPropertyChanged
    {
        private StatisticModel _statisticModel { get; set; }
        public RelayCommands ExtensiveInfoPersonComand { get; set; }
        public List<PersonScoreCover> Persons => _statisticModel.GetPersons();

        public Visibility ContentVisibility
        {
            get { return Persons.Count > 0 ? Visibility.Visible : Visibility.Hidden; }
        }

        public StatisticsViewModel(StatisticModel statisticModel)
        {
            _statisticModel = statisticModel;
            ExtensiveInfoPersonComand = new RelayCommands(ExtensiveInfoPerson);
        }

        private void ExtensiveInfoPerson()
        {
            MessageBox.Show("Розширена інформація по людині коли стояла в наряді в які наряди може заступити мОЖЕ ДОБАВИТИ ? КРОМЕ СКОРА ДОДАТКОВУ іНФОРМАЦіЮ КОМЕНТ НАРЯду позитивною відзнакою негативною нейтральною і кольорами підсвічувати");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
