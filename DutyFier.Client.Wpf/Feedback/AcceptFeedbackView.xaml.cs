﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace DutyFier.Client.Wpf.Feedback
{
    public partial class AcceptFeedbackView : Window
    {
        public AcceptFeedbackView(AcceptFeedbackViewModel acceptFeedbackViewModel)
        {
            InitializeComponent();
            DataContext = acceptFeedbackViewModel;
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
