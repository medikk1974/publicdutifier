﻿using DutyFier.Client.Wpf.Settings.MainSettings;
using DutyFier.Core;
using DutyFier.Core.Models;
using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using Unity;

namespace DutyFier.Client.Wpf.Settings
{
    public class SettingsViewModel
    {
        public IUnityContainer Container { get; }

        private ObservableCollection<object> _children;
        public ObservableCollection<object> Children { get { return _children; } }

        public TabItem SelectTabItem;
        public Object SelectedItem
        {
            set { if (value is TabItem)
                {
                    MyAction();
                    SelectTabItem = (TabItem)value;
                }
            }
        }

        public SettingsViewModel(IUnityContainer container,ExecutorsViewModel ExecutorsViewModel, EditPositionsViewModel EditPositionsViewModel, PositionsViewModel PositionsViewModel, DeysWeightViewModel DeysWeightViewModel, SettingsModel settingsModel)
        {
            Container = container;
            _children = new ObservableCollection<object>();
            _children.Add(ExecutorsViewModel);
            _children.Add(EditPositionsViewModel);
            _children.Add(PositionsViewModel);
            _children.Add(DeysWeightViewModel);
        }


        //todo треба переглянути як правельныше поступити в даный ситуації?
        private void MyAction()
        {
            var SettingsModel =  Container.Resolve<SettingsModel>();
            Children.Clear();
            _children.Add(new ExecutorsViewModel(Container, SettingsModel));
            _children.Add(new EditPositionsViewModel(SettingsModel, Container.Resolve<PersonRepository>())) ;
            _children.Add(new PositionsViewModel(Container, SettingsModel, Container.Resolve<PersonRepository>()));
            _children.Add(new DeysWeightViewModel(SettingsModel));
        }
    }
}