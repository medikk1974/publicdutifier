﻿using DutyFier.Core.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using Unity.Interception.Utilities;

namespace DutyFier.Client.Wpf.Feedback
{
    public class AcceptFeedbackViewModel : INotifyPropertyChanged
    {
        public Duty Duty { get; set; }
        public Executor SelectedExecutor { get; set;}
        public Person SelectedPerson { get; set;}
        public ObservableCollection<Executor> AllExecutor { get; set; }
        public ObservableCollection<Person> FullPersons { get; set; }
        private Dictionary<Executor, Person> ChangeExecutorPerson { get; set; }
        private Dictionary<Executor, double> ChangeExecutorPreliminaryScore { get; set; }
        private List<Executor> DeleteExecutors { get; set; }
        public RelayCommands AcceptComand { get; set; }
        public RelayCommands CancelComand { get; set; }
        public RelayCommand AddExecutorsComand { get; set; }
        public RelayCommands<Executor> DeleteExecutorsComand { get; set; }
        public RelayCommands<Person> PersonChangedCmd { get; set; }

        public AcceptFeedbackViewModel(Duty duty)
        {
            Duty = duty;
            SelectedExecutor = duty.Executors[0];
            FullPersons = new ObservableCollection<Person>(Duty.Executors.Select(e=> e.Person));

            ChangeExecutorPerson = new Dictionary<Executor, Person>();

            ChangeExecutorPreliminaryScore = Duty.Executors.ToDictionary(e => e, e => e.PreliminaryScore);
            DeleteExecutors = new List<Executor>();

            DeleteExecutorsComand = new RelayCommands<Executor>(DeleteExecutor, e=> true);
            PersonChangedCmd = new RelayCommands<Person>(ChangePerson, e => true);
            AcceptComand = new RelayCommands(AcceptChangeExecutor, () => true);
            CancelComand = new RelayCommands(Cancel, () => true);
            AllExecutor = new ObservableCollection<Executor>(Duty.Executors.Select(e => e));

        }


        public void DeleteExecutor(Executor executor)
        {
            AllExecutor.Remove(executor);
        }
        public void ChangePerson(Person person)
        {
            try
            {
                ChangeExecutorPerson.Add(SelectedExecutor, person);
            }
            catch
            {
                ChangeExecutorPerson[SelectedExecutor] = person;
            }
        }
        private void AcceptChangeExecutor()
        {
            ChangeExecutorPerson.ForEach(change => {
                var executor = Duty.Executors.First(ex => ex == change.Key);
                executor.Person = change.Value;
            });
        }
        private void Cancel()
        {
            ChangeExecutorPreliminaryScore.ForEach(e =>
            {
                var temp = Duty.Executors.First(executor => executor == e.Key);
                if (temp != null)
                {
                    temp.PreliminaryScore = e.Value;
                }
            });

            var t = AllExecutor.Select(e => e).ToList();
            AllExecutor.Clear();
            t.ForEach(e => AllExecutor.Add(e));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
