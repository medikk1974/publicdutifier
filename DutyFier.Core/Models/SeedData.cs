﻿using DutyFier.Core.Entities;
using DutyFier.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DutyFier.Core.Models
{
    public class SeedData
    {
        public static void StartData(IRepository<Person> personRepository)
        {
            var persons = new List<Person>()
            {
                new Person("Batienko", "Andree", 1),
                new Person("Vinitckiy", "Sergey", 1),
                new Person("Getmanenko", "Vitaliy", 1),
                new Person("Juruk", "Artem", 1),
                new Person("Juravel", "Andree", 1),
                new Person("Kovalenko", "Andree", 1),
                new Person("Salykov", "Illy", 1),
                new Person("Skrupnik", "Daniil", 1),
                new Person("Fedor", "Vitaly", 1),
                new Person("Shevchuk", "Andrey", 1),
                new Person("Shevchuk", "Nikolay", 1),
                new Person("Shkorupskiy", "Valintin", 1),
                new Person("Zlobin", "Oleksandr", 1),
                new Person("Gumenuk", "Oleksandr", 1),
                new Person("Polomarchuk", "Taras", 1),
                new Person("Kravchuk", "Andrey", 1),
                new Person("Kucher", "Dmitrii", 1),
                new Person("Latosh", "Oleksandr", 1),
                new Person("Timoshov", "Anton", 1),
            };

            // TODO: receive PersonRepository through DI
           
            if (!personRepository.GetAll().Any())
            {
                personRepository.AddRange(persons);
            }
        }
    }
}
