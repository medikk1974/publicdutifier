﻿using DutyFier.Client.Wpf.State;
using System.Windows;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Generate
{
    /// <summary>
    /// Логика взаимодействия для ResultView.xaml
    /// </summary>
    public partial class ResultView : UserControl
    {
        public ResultView(GenerateContext context)
        {
            context.GeneratorRunWhereNoDutys();
            InitializeComponent();
            DataContext = new ResultViewModel(context);
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
