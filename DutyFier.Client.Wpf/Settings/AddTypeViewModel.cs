﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using Unity;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DutyFier.Core;

namespace DutyFier.Client.Wpf.Settings
{
    public class AddTypeViewModel : INotifyPropertyChanged
    {
        public DutyType _dutyType;
        private AdTypeModel AddTypeModel;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public AddTypeViewModel(AdTypeModel adTypeModel)
        {
            _dutyType = new DutyType();
            AddCommand = new RelayCommands(OnAdd, ()=>true);
            AddTypeModel = adTypeModel;
        }
        public string Name { get; set; }
        public RelayCommands AddCommand { get; set; }

        public void OnAdd()
        {
            if (Name != null && !Name.Equals(""))
            {
                _dutyType = new DutyType();
                _dutyType.Name = Name;
                Name = "";
                OnPropertyChanged(nameof(Name));
                AddTypeModel.AddDutyTypeToDB(_dutyType);
            }
        }
        public bool CanAdd()
        {
            return true;
        }
    }
}
