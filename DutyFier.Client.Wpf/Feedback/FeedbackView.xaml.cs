﻿using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Feedback
{
    /// <summary>
    /// Логика взаимодействия для FeedbackView.xaml
    /// </summary>
    public partial class FeedbackView : UserControl
    {
        public FeedbackView(FeedbackViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
        }
    }
}
