﻿using DutyFier.Core.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace DutyFier.Core.Models.ImpoerData
{
    public class ImportDataCsv
    {
        public List<Person> GetPersons(string path)
        {
            List<Person> people = new List<Person>();
            string line;
            StreamReader file = new StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                people.Add(ParseStingInPerson(line));
            }
            return people;
        }

        private Person ParseStingInPerson(string str)
        {
            var arr = str.Split(';');
            if (arr.Length == 3)
            {
                return new Person(arr[0], arr[1], 1);
            }
            throw new Exception();
        }
    }
}
