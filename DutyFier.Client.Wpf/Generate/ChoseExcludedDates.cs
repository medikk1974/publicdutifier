﻿using DutyFier.Client.Wpf.State;
using DutyFier.Core.Entities;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using Unity.Interception.Utilities;

namespace DutyFier.Client.Wpf.Generate
{
    class ChoseExcludedDates : INotifyPropertyChanged
    {

        private ObjectForSlectedDatesDto<Person> _selectedPersonSelectDateDto { get; set; }
        public ObjectForSlectedDatesDto<Person> SelectedPersonSelectDateDto 
        {
            get { return _selectedPersonSelectDateDto; }
            set 
            {
                _selectedPersonSelectDateDto = value;
                if (_selectedDatesCollection != null)
                {
                    flug = false;
                    _selectedDatesCollection.Clear();
                    SelectedPersonSelectDateDto.SelectedDates.ForEach(e => _selectedDatesCollection.Add(e));
                    flug = true;
                }
                OnPropertyChanged("SelectedPersonSelectDateDto");
            }
        }
        public ObservableCollection<ObjectForSlectedDatesDto<Person>> PersonDatesDto {get;set;}

        public RelayCommands<SelectedDatesCollection> GetSelectedDatesCollectionComand { get; set; }
        public RelayCommand ComadAcpet { get; }
        public RelayCommand ComadClear { get; }


        private SelectedDatesCollection _selectedDatesCollection;
        public GenerateContext Context { get; set; }

        public ChoseExcludedDates(GenerateContext generateContext)
        {
            PersonDatesDto = new ObservableCollection<ObjectForSlectedDatesDto<Person>>();
            GetSelectedDatesCollectionComand = new RelayCommands<SelectedDatesCollection>(SelectDateColetion);
            Context = generateContext;
            ConvertToPersonSelectDateDto();

            ComadAcpet = new RelayCommand(AcceptSelectDates);
            ComadClear = new RelayCommand(ClearSelectDates);

            SelectedPersonSelectDateDto = PersonDatesDto.FirstOrDefault();
        }

        private void ClearSelectDates(object obj)
        {
            _selectedDatesCollection.Clear();
            SelectedPersonSelectDateDto.SetDates(_selectedDatesCollection);
            ConvertToGenerateContextData();
        }

        private void AcceptSelectDates(object obj)
        {
            SelectedPersonSelectDateDto.SetDates(_selectedDatesCollection);
            OnPropertyChanged("SelectedPersonSelectDateDto");
            ConvertToGenerateContextData();
        }

        public bool flug = true;
        private void SelectDateColetion(SelectedDatesCollection obj)
        {
        


            _selectedDatesCollection = obj;

            if (SelectedPersonSelectDateDto != null)
            {
                SelectedPersonSelectDateDto.SelectedDates.ToList().ForEach(e => _selectedDatesCollection.Add(e));
            }
            AcceptSelectDates(null);


            obj.CollectionChanged += (h, b) =>
            {
                if (flug)
                {
                    AcceptSelectDates(null);
                }
            };
        }

        private void ConvertToPersonSelectDateDto()
        {
            Context.ExludeDates.ForEach(e =>
            {
                var t = new ObjectForSlectedDatesDto<Person>()
                {
                    Id = e.Key.Id,
                    Key = e.Key,
                    Name = e.Key.FirstName + " " + e.Key.LastName,
                    SelectedDates = new System.Collections.ObjectModel.ObservableCollection<DateTime>(e.Value.ToList())
                };
                PersonDatesDto.Add(t);
            });
        }

        private void ConvertToGenerateContextData()
        {
            PersonDatesDto.ForEach(e =>
            {
                Context.ExludeDates[e.Key] = e.SelectedDates.ToList();
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
