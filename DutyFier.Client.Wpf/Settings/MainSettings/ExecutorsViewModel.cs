﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DutyFier.Core.Models.ImpoerData;
using System.Windows;
using System.Windows.Controls;
using Unity;

namespace DutyFier.Client.Wpf.Settings.MainSettings
{
    public class ExecutorsViewModel : INotifyPropertyChanged
    {
        public RelayCommands LoadedWindowCommand { get; set; }
        private Person selectedExecutor;
        private IUnityContainer container;
        public ExecutorsViewModel(IUnityContainer unityContainer ,SettingsModel settingsModel)
        {
            // DutyFierContext = MainWindowViewModel.Container.Resolve<DutyFierContext>();
            SettingsModel = settingsModel;
            container = unityContainer; 
            People = new ObservableCollection<Person>(SettingsModel.GetAllPerson());
            AddExecutorCommand = new RelayCommands(addExecutorCommand, Can);
            RemovePersonCommand = new RelayCommands(removePersonCommand, Can);

            ImportDataComand = new RelayCommands(ImportData);
            TestDataComand = new RelayCommands(SeedTestData);
            LoadedWindowCommand = new RelayCommands(Loaded);
        }

        private void SeedTestData()
        {
           
        }

        private void ImportData()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.csv";
            if (openFileDialog.ShowDialog() == true)
            {
                //TODO : lOADERdATA ELIPS
                var import = new ImportDataCsv();
                try
                {
                    var importsPerson = import.GetPersons(openFileDialog.FileName);
                    importsPerson.ForEach(e => People.Add(e));
                    SettingsModel.AddPersonRange(importsPerson);
                }
                catch
                {
                    MessageBox.Show("This file hase no valid data");
                }
            }
        }

        public Person SelectedExecutor
        {
            get
            {
                return selectedExecutor;
            }
            set
            {
                selectedExecutor = value;
                OnPropertyChanged("SelectedExecutor");
            }
        }

        public SettingsModel SettingsModel { get; set; }
        public ObservableCollection<Person> People { get; set; }
        public RelayCommands AddExecutorCommand { get; set; }
        public RelayCommands RemovePersonCommand { get; set; }

        
        public RelayCommands ImportDataComand { get; set; }
        public RelayCommands TestDataComand { get; set; }
        public void Loaded()
        {
            
        }

        private void addExecutorCommand()
        {
            
            AddPersonView apv = container.Resolve<AddPersonView>();
            if (apv.ShowDialog() == true)
            {
                People = new ObservableCollection<Person>(SettingsModel.GetAllPerson());
                OnPropertyChanged("People");
                apv.Close();
            }
        }

        private void removePersonCommand()
        {
            SettingsModel.RemovePerson(SelectedExecutor);
            People.Remove(SelectedExecutor);
        }

        public bool Can()
        {
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}