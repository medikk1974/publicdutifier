﻿using DutyFier.Core.Models;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Unity;

namespace DutyFier.Client.Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(MainWindowViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void MoveSelecter(int index)
        {
            TransitionEff.OnApplyTemplate();
            GridSelecter.Margin = new Thickness(0, (100 + (60 * index)), 0, 0);
        }

        private void ListVievButtons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MoveSelecter(ListVievButtons.SelectedIndex);
        }

        private void Window1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

    }
}

