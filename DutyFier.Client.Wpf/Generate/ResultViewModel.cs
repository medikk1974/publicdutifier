﻿using DutyFier.Client.Wpf.State;
using DutyFier.Core.Entities;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DutyFier.Client.Wpf.Generate
{
    class ResultViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public ObservableCollection<Duty> Dutys { get; set; }
        public Duty SelectedDuty { get; set; }
        public GenerateContext context { get; set; }
        public RelayCommands ComandChangeExecutors { get; set; }
        public ResultViewModel(GenerateContext context)
        {
            this.context = context;
            Dutys = context.duties;
            ComandChangeExecutors = new RelayCommands(ShowDialogWindowChangeDuty, () => true);
        }

        private void ShowDialogWindowChangeDuty()
        {
            var dataContext = new EditDutyViewModel(SelectedDuty, context.ExludeDates);
            EditDutyView apv = new EditDutyView(dataContext);
            
            if (apv.ShowDialog() == true)
            {
                var temp = Dutys.Select(e => e).ToList() ;
                Dutys.Clear();
                temp.ForEach(e => Dutys.Add(e));
                context.Update();
            }
            
            else if (apv.DialogResult == false)
            {
                apv.Close();
            }
        }
    }
}
