﻿using DutyFier.Core.Entities;
using DutyFier.Core.Interfaces;

namespace DutyFier.Core.Models
{
    public class AddPositionModel
    {
        public AddPositionModel(IRepository<Position> positionRepository)
        {
            PositionRepository = positionRepository;
        }

        private IRepository<Position> PositionRepository { get; set;}
        public void AddPositionToDB(Position _position)
        {
            PositionRepository.Create(_position);
        }
    }
}
