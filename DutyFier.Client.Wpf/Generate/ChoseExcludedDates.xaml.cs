﻿using System.Windows;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Generate
{
    public partial class ChoseExcludedDatesAndHolydaysView : UserControl
    {
        public ChoseExcludedDatesAndHolydaysView(State.GenerateContext context)
        {
            InitializeComponent();
            DataContext = new ChoseExcludedDates(context);
        }

    }
}
