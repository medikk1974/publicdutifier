﻿using DutyFier.Client.Wpf.State;
using DutyFier.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using DutyFier.Core.Models;
using System.Linq;
using System.Collections.ObjectModel;
using Unity.Interception.Utilities;
using System.ComponentModel.DataAnnotations;

namespace DutyFier.Client.Wpf.Generate
{
    class SelectDatesViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<ObjectForSlectedDatesDto<DutyType>> DutyTypes { get; set; }

        private ObjectForSlectedDatesDto<DutyType> _SelectDutyTypes;
        public ObjectForSlectedDatesDto<DutyType> SelectDutyTypes 
        {
            get
            {
                return _SelectDutyTypes;
            }
            set 
            {
                _SelectDutyTypes = value;

                if (selectDates != null)
                {
                    flug = false;
                    selectDates.Clear();
                    _SelectDutyTypes.SelectedDates.ForEach(e => selectDates.Add(e));
                    flug = true;
                }

                OnPropertyChanged("SelectDutyTypes");
            }
        }
        public GenerateContext context { get; set; }
        private SelectedDatesCollection selectDates { get; set; }
        public Dictionary<DutyType, List<DateTime>> DatesDutyTypes { get; set; }
        public RelayCommands<SelectedDatesCollection> GetSelectedDatesCollectionComand { get; set; }
        public RelayCommand ComadAcpet { get; }
        public RelayCommand ComadClear { get; }

        public SelectDatesViewModel(GenerateContext generateContext)
        {
            GetSelectedDatesCollectionComand = new RelayCommands<SelectedDatesCollection>(SelectDateColetion);
            context = generateContext;
            DutyTypes = new ObservableCollection<ObjectForSlectedDatesDto<DutyType>>();
            DatesDutyTypes = generateContext.DutyTypeDate;


            ConvertToDutysForSlectedDatesDto();

            ComadAcpet = new RelayCommand(AcceptSelectDates);
            ComadClear = new RelayCommand(ClearSelectDates);
            SelectDutyTypes = DutyTypes.First();
        }

        private void ClearSelectDates(object obj)
        {
            selectDates.Clear();
            SelectDutyTypes.SetDates(SelectDates);
            ConvertToGenerateContextData();
        }

        private void AcceptSelectDates(object obj)
        {
            SelectDutyTypes.SetDates(selectDates);
            OnPropertyChanged("SelectDutyTypes");
            ConvertToGenerateContextData();
        }

        public SelectedDatesCollection SelectDates
        {
            get => selectDates;
            set
            {
                selectDates = value;
                selectDates.CollectionChanged += (e, s) =>
                {
                };
                OnPropertyChanged(nameof(SelectDates));
            }
        }

        private void ConvertToDutysForSlectedDatesDto()
        {
            DatesDutyTypes.ForEach(e => DutyTypes.Add(new ObjectForSlectedDatesDto<DutyType>
            {
                Key = e.Key,
                SelectedDates = new ObservableCollection<DateTime>(e.Value.ToList()),
                Id = e.Key.Id,
                Name = e.Key.Name,
            }));
        }

        private void ConvertToGenerateContextData()
        {
            DutyTypes.ForEach(e =>
            {
                context.DutyTypeDate[e.Key] = e.SelectedDates.ToList();
            });
        }

        private bool flug = true;
        private void SelectDateColetion(SelectedDatesCollection obj)
        {
            selectDates = obj;

            if (SelectDutyTypes != null)
            {
                SelectDutyTypes.SelectedDates.ForEach(e => selectDates.Add(e));
            }
            obj.CollectionChanged += (h, b) =>
            {
                if (flug)
                {
                    AcceptSelectDates(null);
                }
            };
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
    }




    public class ObjectForSlectedDatesDto<T> : INotifyPropertyChanged
    {
        public T Key { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public ObservableCollection<DateTime> SelectedDates { get; set; }

        public void SetDates(ICollection<DateTime> dataTimes)
        {
            SelectedDates.Clear();
            dataTimes.ForEach(e => SelectedDates.Add(e));
            OnPropertyChanged(nameof(SelectedDates));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
}