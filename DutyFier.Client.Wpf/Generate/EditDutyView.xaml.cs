﻿using System.Windows;
using System.Windows.Input;

namespace DutyFier.Client.Wpf.Generate
{
    /// <summary>
    /// Логика взаимодействия для EditDutyView.xaml
    /// </summary>
    public partial class EditDutyView : Window
    {
        public EditDutyView(EditDutyViewModel editDutyViewModel)
        {
            InitializeComponent();
            DataContext = editDutyViewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
