﻿using DutyFier.Client.Wpf.Generate;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.State
{
    class DatesExcludingState : IGenerationState
    {
        public UserControl CurrentStateControl { get; set; }
        public GenerateContext Context { get; set; }
        public bool IsBackwardStateAllowed { get; set; }
        public bool IsForwardStateAllowed { get; set; }
        
        public DatesExcludingState( GenerateContext context)
        {
            IsBackwardStateAllowed = true;
            IsForwardStateAllowed = true;
            Context = context;
            CurrentStateControl = new ChoseExcludedDatesAndHolydaysView(context);
        }

        public IGenerationState GoBackward()
        {
            return new DatesSelectionState(Context);
        }

        public IGenerationState GoForward()
        {
            return new PreviewState(Context);
        }
    }
}
