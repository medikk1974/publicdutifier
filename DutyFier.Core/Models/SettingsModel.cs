﻿using DutyFier.Core.Entities;
using DutyFier.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DutyFier.Core.Models
{
    public class SettingsModel
    {
        private IRepository<Person> PersonRepository { get; set; }
        private IRepository<Position> PositionRepository { get; set; }
        private IRepository<DaysOfWeekWeight> DaysOfWeekWeightRepository { get; set; }

        public SettingsModel(IRepository<Person> personRepository, IRepository<Position> positionRepository, IRepository<DaysOfWeekWeight> daysOfWeekWeightRepository)
        {
            PersonRepository = personRepository;
            PositionRepository = positionRepository;
            DaysOfWeekWeightRepository = daysOfWeekWeightRepository;
        }
        public void AddPersonRange(List<Person> peoples){
            PersonRepository.AddRange(peoples);
        }
        public void RemovePosition(Position position) => PositionRepository.Delete(position);

        public void RemovePerson(Person person) => PersonRepository.Delete(person);

        public void UpdatePersonDependencyToPosition(Person selectedPerson)
        {
            PersonRepository.Update(selectedPerson);
        }

        public void SeedTestData()
        {
            //TODO LOADER;
            SeedData.StartData(PersonRepository);
        }

        public void UpdatePositionDependencyToPerson(List<Position> positions)
        {
            positions.ForEach(a => PositionRepository.Update(a));
        }

        public void UpdateDateWeight(DaysOfWeekWeight daysOfWeekWeight, double weight)
        {
            daysOfWeekWeight.Weight = weight;
            DaysOfWeekWeightRepository.Update(daysOfWeekWeight);
        }

        public List<DaysOfWeekWeight> GetAllDaysOfWeek() => DaysOfWeekWeightRepository.GetAll().ToList(); 
        public List<Position> GetAllPosition() => PositionRepository.GetAll().ToList();
        public List<Person> GetAllPerson() => PersonRepository.GetAll().ToList();

        public void CahngeWeightDays(DaysOfWeekWeight daysOfWeekWeight)
        {
            DaysOfWeekWeightRepository.Update(daysOfWeekWeight);
        }
    }
}