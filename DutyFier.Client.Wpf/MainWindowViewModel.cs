﻿using DutyFier.Client.Wpf.Feedback;
using System;
using System.Windows;
using System.Windows.Controls;
using DutyFier.Client.Wpf.Statistics;
using DutyFier.Client.Wpf.Settings;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Unity;
using DutyFier.Core;
using DutyFier.Core.Models;
using DutyFier.Client.Wpf.ListOfDutys;
using DutyFier.Client.Wpf.Services;
using DutyFier.Client.Wpf.Generate;

namespace DutyFier.Client.Wpf
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private UIElementCollection childrenPanel { get; set; }
        private MainWindowModel MainWindowModel { get; set; }

        private WindowState windowState;
        private bool isDarkModeOn;
        public IUnityContainer Container { get; set; }
        Action<int> updateFidbackCountAction;
        
        public WindowState WindowState
        {
            get
            {
                return windowState;
            }

            set
            {
                windowState = value;
                OnPropertyChanged(nameof(WindowState));
            }
        }
        public bool IsDarkModeOn
        {
            get
            {
                return isDarkModeOn;
            }

            set
            {
                isDarkModeOn = value;
                OnPropertyChanged(nameof(IsDarkModeOn));
            }
        }
        public bool IsListViewEnabled{get;set;}
        public PersonModel personModel { get; set; }
        public RelayCommands<UIElementCollection> WindowLoadedCommand { get; set; }
        public RelayCommands<UIElementCollection> GoToListOfDutyCommand { get; set; }
        public RelayCommands<UIElementCollection> GoToStatisticsCommand { get; set; }
        public RelayCommands<UIElementCollection> GoToSetingCommand { get; set; }
        public RelayCommands<UIElementCollection> GotToGenerateCommand { get; set; }
        public RelayCommands<UIElementCollection> GoToFeedbackCommand { get; set; }
        public RelayCommands MinimizeCommand { get; set; }
        public RelayCommands ChangeWindowStateCommand { get; set; }
        public RelayCommands DarkModeCommand { get; set; }
        public RelayCommands PowerOffCommand { get; set; }

        private ChangeUiServices _changeUiServices;
        public MainWindowViewModel(IUnityContainer container, ChangeUiServices changeUiServices, DutyFierContext context, PersonRepository personRepository, DutyRepository dutyRepository)
        {
            _changeUiServices = changeUiServices;

            Container = container;

            DefaultDaysAndPositionBDWrite.WriteToData(context);
            personModel = new PersonModel(personRepository);
            WindowLoadedCommand = new RelayCommands<UIElementCollection>(windowLoadedCommand);

            GoToListOfDutyCommand = new RelayCommands<UIElementCollection>(listCommand);
            GoToStatisticsCommand = new RelayCommands<UIElementCollection>(statisticCommand);
            GoToSetingCommand = new RelayCommands<UIElementCollection>(settingsCommand);
            GotToGenerateCommand = new RelayCommands<UIElementCollection>(generateCommand);
            GoToFeedbackCommand = new RelayCommands<UIElementCollection>(feedbackCommand);
         
            MinimizeCommand = new RelayCommands(minimizeCommand);
            ChangeWindowStateCommand = new RelayCommands(changeWindowStateCommand);
            DarkModeCommand = new RelayCommands(darkModeCommand);
            PowerOffCommand = new RelayCommands(powerOffCommand);
        
            IsDarkModeOn = false;
            //TODO delete in future
            MainWindowModel = new MainWindowModel(dutyRepository);
            _FeedbacksCount = MainWindowModel.GetUncreatedFeedbackCount();
            updateFidbackCountAction = new Action<int>(UpdateCountOfUncreatedFeedback);

        }


        private void windowLoadedCommand(UIElementCollection obj)
        {
            _changeUiServices.SetScen(obj);

            childrenPanel = obj;
            IsListViewEnabled = true;


            if (personModel.GetAllPerson().Count == 0)
            {
               
                OnPropertyChanged(nameof(IsListViewEnabled));
                _changeUiServices.CangeUserControl(Container.Resolve<SettingsView>());
                MessageBox.Show("There is no executors to work, please add executors and restart programm");
            }
            else
            {
                OnPropertyChanged(nameof(IsListViewEnabled));
                _changeUiServices.CangeUserControl(Container.Resolve<StatisticsView>());
            }
        }

        public void listCommand(UIElementCollection obj)
        {
            _changeUiServices.CangeUserControl(Container.Resolve<ListOfDutysView>());           
        }
        public void powerOffCommand()
        {
            Container.Resolve<DutyFierContext>().Dispose();
            Application.Current.Shutdown();
        }
        public void darkModeCommand()
        {
            if (isDarkModeOn)
            {
                Uri uri = new Uri($"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/materialdesigntheme.dark.xaml");
                System.Windows.Application.Current.Resources.MergedDictionaries.RemoveAt(0);
                System.Windows.Application.Current.Resources.MergedDictionaries.Insert(0, new ResourceDictionary() { Source = uri });
                Uri uri2 = new Uri($"pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Primary/materialdesigncolor.teal.xaml");
                System.Windows.Application.Current.Resources.MergedDictionaries.RemoveAt(2);
                System.Windows.Application.Current.Resources.MergedDictionaries.Insert(2, new ResourceDictionary() { Source = uri2 });
                IsDarkModeOn = true;
            }
            else
            {
                Uri uri = new Uri($"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/materialdesigntheme.light.xaml");
                System.Windows.Application.Current.Resources.MergedDictionaries.RemoveAt(0);
                System.Windows.Application.Current.Resources.MergedDictionaries.Insert(0, new ResourceDictionary() { Source = uri });
                Uri uri2 = new Uri($"pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Primary/materialdesigncolor.blue.xaml");
                System.Windows.Application.Current.Resources.MergedDictionaries.RemoveAt(2);
                System.Windows.Application.Current.Resources.MergedDictionaries.Insert(2, new ResourceDictionary() { Source = uri2 });
                IsDarkModeOn = false;
            }
        }
        public void changeWindowStateCommand()
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
            }
            else
            {
                WindowState = WindowState.Normal;
            }
        }
        public void minimizeCommand()
        {
            WindowState = WindowState.Minimized;
        }
        public void statisticCommand(UIElementCollection obj)
        {
            var statistic = Container.Resolve<StatisticsView>();
            _changeUiServices.CangeUserControl(statistic);
        }
        public void settingsCommand(UIElementCollection obj)
        {
            var settings = Container.Resolve<SettingsView>();
            _changeUiServices.CangeUserControl(settings);
        }
        public void generateCommand(UIElementCollection obj)
        {
            var containerForGeneratesUserControls = Container.Resolve<ContainerForGeneratesUserControls>();
            _changeUiServices.CangeUserControl(containerForGeneratesUserControls);
        }
        public void feedbackCommand(UIElementCollection obj)
        {
            var feedbackViewModel = Container.Resolve<FeedbackView>();
            ((FeedbackViewModel)feedbackViewModel.DataContext)._updateFidbackCountLable = updateFidbackCountAction;
            _changeUiServices.CangeUserControl(feedbackViewModel);

        }
     
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private int _FeedbacksCount = 0;
        public int FeedbacksCount
        {
            get
            {
                var t = _FeedbacksCount;
                return t;
            }
        }

        public void UpdateCountOfUncreatedFeedback(int i)
        {
            _FeedbacksCount = i;
            OnPropertyChanged(nameof(FeedbacksCount));
        }
    }
}
