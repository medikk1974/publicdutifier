﻿using DutyFier.Client.Wpf.LoadIndecator;
using DutyFier.Client.Wpf.Services;
using DutyFier.Client.Wpf.Settings;
using DutyFier.Core;
using DutyFier.Core.Models;
using System;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using DutyFier.Core.Interfaces;
using Unity;
using DutyFier.Core.Entities;
using DutyFier.Client.Wpf.Feedback;
using DutyFier.Client.Wpf.Generate;

namespace DutyFier.Client.Wpf
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected async override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            IUnityContainer container = new UnityContainer();

            container.RegisterInstance<IUnityContainer>(container, InstanceLifetime.Singleton);

            container.RegisterType<DutyFierContext>();
            container.RegisterType<PersonRepository>();
            container.RegisterType<DutyRepository>();
            container.RegisterType<MainWindowViewModel>();
            container.RegisterType<MainWindow>();
            container.RegisterType<FeedbackViewModel>();
            container.RegisterType<ContainerForGeneratesUserControlsViewModel>();
            container.RegisterType<AddTypeViewModel>();
            container.RegisterType<AddTypeView>();
            container.RegisterType<AdTypeModel>();

            

            container.RegisterType<SettingsView>();
            container.RegisterType<SettingsModel>();

            container.RegisterType<IRepository<DutyType>, DutyTypeRepository>();

          
            container.RegisterType<IRepository<Person>,PersonRepository>();
            container.RegisterType<IRepository<PersonDutyFeedback>, PersonDutyFeedbackRepository>();
            container.RegisterType<IRepository<Position>,PositionRepository>();
            container.RegisterType<IRepository<DaysOfWeekWeight>, DaysOfWeekWeightRepository>();
            //container.RegisterType<IRepository<Person>, PersonRepository>();



            container.RegisterType<ChangeUiServices>(TypeLifetime.Singleton);
            container.RegisterType<DutyFierContext>(TypeLifetime.Singleton);

            container.AddExtension(new Diagnostic());


            var loading = new LoadingIndecatorUi();
            loading.Show();

            await Task.Run(() =>
            {
                container.Resolve<DutyFierContext>().Persons.Any();
            });
           
            container.Resolve<MainWindow>().Show();
            loading.Close();
        }
    }
}
