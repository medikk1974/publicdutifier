﻿using System.Collections.Generic;

namespace DutyFier.Core.Entities
{
    public class Executor
    {
        public int Id { get; set; }
        public int PositionId { get; set; }
        public virtual Position Position { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int DutyId { get; set; }
        public virtual Duty Duty { get; set; }
        public double PreliminaryScore { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Executor executor &&
                   Id == executor.Id &&
                   PositionId == executor.PositionId &&
                   EqualityComparer<Position>.Default.Equals(Position, executor.Position) &&
                   PersonId == executor.PersonId &&
                   EqualityComparer<Person>.Default.Equals(Person, executor.Person) &&
                   DutyId == executor.DutyId &&
                   EqualityComparer<Duty>.Default.Equals(Duty, executor.Duty) &&
                   PreliminaryScore == executor.PreliminaryScore;
        }

        public override int GetHashCode()
        {
            int hashCode = -1531396286;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + PositionId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Position>.Default.GetHashCode(Position);
            hashCode = hashCode * -1521134295 + PersonId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Person>.Default.GetHashCode(Person);
            hashCode = hashCode * -1521134295 + DutyId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Duty>.Default.GetHashCode(Duty);
            hashCode = hashCode * -1521134295 + PreliminaryScore.GetHashCode();
            return hashCode;
        }
    }
}
