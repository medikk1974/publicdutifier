﻿using System.Windows;
using System.Windows.Input;

namespace DutyFier.Client.Wpf.Settings
{
    /// <summary>
    /// Логика взаимодействия для AddTypeView.xaml
    /// </summary>
    public partial class AddTypeView : Window
    {
        public AddTypeView(AddTypeViewModel addTypeViewModel)
        {
            InitializeComponent();
            DataContext = addTypeViewModel;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
