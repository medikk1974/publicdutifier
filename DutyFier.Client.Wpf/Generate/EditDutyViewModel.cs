﻿using DutyFier.Core.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Unity.Interception.Utilities;

namespace DutyFier.Client.Wpf.Generate
{
    public class EditDutyViewModel : INotifyPropertyChanged
    {
        public Duty ChangeDuty { get; set;}
        public Executor SelectedExecutor { get; set; }
        public ObservableCollection<Person> FullPersons { get; set; }
        private Dictionary<Executor, Person> ChangeExecutor { get; set; }
        public RelayCommands OkComand { get; set; }
        public RelayCommands<Person> PersonChangedCmd { get; set; }

        public EditDutyViewModel(Duty duty, Dictionary<Person, List<DateTime>> exludeDates)
        {
            ChangeDuty = duty;
            FullPersons = new ObservableCollection<Person>(exludeDates.Select(e => e.Key));
            PersonChangedCmd = new RelayCommands<Person>(ChangePerson, e=> true);
            OkComand = new RelayCommands(AcceptChangeExecutor, () => true);
            ChangeExecutor = new Dictionary<Executor, Person>();
        }

        public void ChangePerson(Person person)
        {
            try
            {
                ChangeExecutor.Add(SelectedExecutor, person);
            }
            catch
            {
                ChangeExecutor[SelectedExecutor] = person;
            }
        }

        private void AcceptChangeExecutor()
        {
            ChangeExecutor.ForEach(change => {
                var executor = ChangeDuty.Executors.First(ex => ex == change.Key);
                executor.Person = change.Value;
            });
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
