﻿using System;

namespace DutyFier.Core.Exeptions
{
    class EmptyRequestException : ArgumentNullException
    {
        public EmptyRequestException(string message) : base(message)
        {
        }
    }
}
