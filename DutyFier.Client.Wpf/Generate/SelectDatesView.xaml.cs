﻿using DutyFier.Client.Wpf.State;
using System.Windows;
using System.Windows.Controls;


namespace DutyFier.Client.Wpf.Generate
{
    /// <summary>
    /// Логика взаимодействия для SelectDatesView.xaml
    /// </summary>
    public partial class SelectDatesView : UserControl
    {
        //public SelectDatesView()
        //{
        //    InitializeComponent();
        //    DataContext = new SelectDatesViewModel();
        //}
        public SelectDatesView(GenerateContext generateContext)
        {
            InitializeComponent();
            DataContext = new SelectDatesViewModel(generateContext);
        }

    }
}
