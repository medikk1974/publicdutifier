﻿using DutyFier.Core.Entities;
using System.Collections.Generic;

namespace DutyFier.Core.Models
{
    public class FeedbacksContext
    {
        public List<PersonDutyFeedback> PersonDutyFeedbacks { get; set; }

        public FeedbacksContext(List<PersonDutyFeedback> personDutyFeedbacks)
        {
            PersonDutyFeedbacks = personDutyFeedbacks;
        }
    }
}
