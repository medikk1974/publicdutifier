﻿using DutyFier.Client.Wpf.Services;
using DutyFier.Client.Wpf.State;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Unity;

namespace DutyFier.Client.Wpf.Generate
{
    public class ContainerForGeneratesUserControlsViewModel : INotifyPropertyChanged
    {
        public bool IsForwardEnable { get; set; } = true;
        public bool IsBackwardEnable { get; set; } = false;

        public RelayCommands<UIElementCollection> WindowLoadedCommand { get; set; }
        private ChangeUiServices _changeUiServices;
        private IUnityContainer _container;
        public IGenerationState state;

        public RelayCommands<UIElementCollection> BackwardCommand { get; }
        public RelayCommands<UIElementCollection> ForwardCommand { get; }

        public ContainerForGeneratesUserControlsViewModel(IUnityContainer container, DatesSelectionState datesSelectionState )
        {
            WindowLoadedCommand = new RelayCommands<UIElementCollection>(windowLoadedCommand);
            _changeUiServices = new ChangeUiServices();
            _container = container;

  
            state = datesSelectionState;

            BackwardCommand = new RelayCommands<UIElementCollection>(backwardCommand);
            ForwardCommand = new RelayCommands<UIElementCollection>(forwardCommand);

            state.Context.ForvardComand = ForwardCommand;
        }


        private void windowLoadedCommand(UIElementCollection obj)
        {
            _changeUiServices.SetScen(obj);
            _changeUiServices.CangeUserControl(state.CurrentStateControl);
        }


        public void forwardCommand(UIElementCollection obj)
        {
            state = state.GoForward();
            _changeUiServices.CangeUserControl(state.CurrentStateControl);
            SetVisibilityForward();
            SetVisibilityBackward();
        }
        public void backwardCommand(UIElementCollection obj)
        {
            state = state.GoBackward();
            _changeUiServices.CangeUserControl(state.CurrentStateControl);
            SetVisibilityForward();
            SetVisibilityBackward();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        private void SetVisibilityBackward()
        {
            if( state.CurrentStateControl is SelectDatesView || state.CurrentStateControl is ResultView)
            {
                IsBackwardEnable = false;
            }
            else
            {
                IsBackwardEnable = true;
            }

            OnPropertyChanged(nameof(IsBackwardEnable));
        }


        private void SetVisibilityForward()
        {
            if (state.CurrentStateControl is ResultView)
            {
                IsForwardEnable = false;
            }
            else
            {
                IsForwardEnable = true;
            }

            OnPropertyChanged(nameof(IsForwardEnable));

        }
    }
}
