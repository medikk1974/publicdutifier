﻿using System.Windows;
using System.Windows.Input;

namespace DutyFier.Client.Wpf.Settings
{
    /// <summary>
    /// Логика взаимодействия для AddPositionView.xaml
    /// </summary>
    public partial class AddPositionView : Window
    {
        public AddPositionView(AddPositionViewModel addPositionViewModel)
        {
            InitializeComponent();
            DataContext = addPositionViewModel;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
