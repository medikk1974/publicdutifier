﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using DutyFier.Core;
using System;
using System.Linq;
using Unity;
using System.Collections.ObjectModel;
using Unity.Interception.Utilities;
using System.Windows;

namespace DutyFier.Client.Wpf.Feedback
{
    public class FeedbackViewModel
    {
        private DutyRepository dutyRepository { get; set; }
        private PersonDutyFeedbackRepository personDutyFeedbackRepository {get;set;}
        public ObservableCollection<Duty> Dutys { get; set; }
        public Duty SelectedDuty { get; set; }
        public RelayCommands CreateAcceptFeedbackViewCommand { get; set; }
        public RelayCommands AcceptAllCommand { get; set; }

        //FeedbackView.FeedbackChangeCountTrigger FeedbackChangeCount { get; set; }
        //public delegate void AcceptFeedbackViewClosingTrigger();

        public Visibility ContentVisibility {
            get { return Dutys.Count > 0 ? Visibility.Visible : Visibility.Hidden; }
        }

        public RelayCommands AcceptFeedbackCommand { get; set; }
        public Action<int> _updateFidbackCountLable;

        public FeedbackViewModel(/*FeedbackView.FeedbackChangeCountTrigger changeCountTrigger*/ DutyRepository DutyRepository, PersonDutyFeedbackRepository PersonDutyFeedbackRepository)
        {
            dutyRepository = DutyRepository; /*new DutyRepository(MainWindowViewModel.Container.Resolve<DutyFierContext>());*/
            personDutyFeedbackRepository = PersonDutyFeedbackRepository;

            Dutys = new ObservableCollection<Duty>(dutyRepository.GetAll().Where(duty => !duty.IsApproved).ToList());
          
            CreateAcceptFeedbackViewCommand = new RelayCommands(CreateAcceptFeedbackView, () => true);
            AcceptAllCommand = new RelayCommands(AcceptAll, () => true);

            //TODO подивитись як працюэ
            //FeedbackChangeCount = changeCountTrigger;

            AcceptFeedbackCommand = new RelayCommands(AcceptFeedback);

            //_updateFidbackCountLable = updateFidbackCountAction;
        }

        private void AcceptFeedback()
        {
            AcceptDuty(SelectedDuty);
            Dutys.Remove(SelectedDuty);
            _updateFidbackCountLable.Invoke(Dutys.Count);
        }

        private void AcceptAll()
        {
            Dutys.ForEach(e => AcceptDuty(e));
            Dutys.Clear();
            _updateFidbackCountLable.Invoke(Dutys.Count);
        }
        private void CreateAcceptFeedbackView()
        {
            var AcceptFeedbackViewModel = new AcceptFeedbackViewModel(SelectedDuty);
            var acceptFeedbackView = new AcceptFeedbackView(AcceptFeedbackViewModel);

            if (acceptFeedbackView.ShowDialog() == true)
            {
                AcceptDuty(SelectedDuty);
                Dutys.Remove(SelectedDuty);
            }
            _updateFidbackCountLable.Invoke(Dutys.Count);
        }

        private void AcceptDuty(Duty duty)
        {
            duty.IsApproved = true;
            var feedbacks = duty.Executors.Select(ex => new PersonDutyFeedback()
            {
                Duty = duty,
                Person = ex.Person,
                Score = ex.PreliminaryScore

            }).ToList();

            dutyRepository.Update(duty);
            personDutyFeedbackRepository.AddRange(feedbacks);
            _updateFidbackCountLable.Invoke(Dutys.Count);
        }
    }
}
