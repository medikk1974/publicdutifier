﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DutyFier.Client.Wpf.Settings.MainSettings
{
    public class DeysWeightViewModel : INotifyPropertyChanged
    {
        public SettingsModel SettingsModel { get; set; }
        private DaysOfWeekWeight _daysOfWeek;
        public RelayCommands ChengeDateWeightCommand { get; set; }
        public ObservableCollection<DaysOfWeekWeight> Days { get; set; }

        public double NewWeight { get; set; } = 0;
        public DaysOfWeekWeight SelectedDays
        {
            get
            {
                return _daysOfWeek;
            }
            set
            {
                OnPropertyChanged("SelectedDays");
                _daysOfWeek = value;
            }
        }
        public DeysWeightViewModel(SettingsModel settingsModel)
        {
            SettingsModel = settingsModel;
            Days = new ObservableCollection<DaysOfWeekWeight>(SettingsModel.GetAllDaysOfWeek());
            ChengeDateWeightCommand = new RelayCommands(ChangeDateWeight);
        }

        private void ChangeDateWeight()
        {
            if(SelectedDays!=null && NewWeight != null)
            {
                SettingsModel.UpdateDateWeight(SelectedDays, NewWeight);
                var temp = Days.ToList();
                Days.Clear();
                temp.ForEach(e => Days.Add(e));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
