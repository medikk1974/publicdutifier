﻿using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using Unity;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DutyFier.Core;

namespace DutyFier.Client.Wpf.Settings
{
    public class AddPersonViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public Person _person;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AddPersonModel AddPersonModel{ get; set; } 
        public AddPersonViewModel(AddPersonModel personModel)
        {
            _person = new Person();
            AddCommand = new RelayCommand(OnAdd);
            AddPersonModel = personModel;
        }
        public RelayCommand AddCommand { get; set; }
        public void OnAdd(object obj)
        {
            if (!FirstName.Equals("")&&!LastName.Equals(""))
            {
                _person = new Person();
                _person.Factor = 1;
                _person.FirstName = FirstName;
                _person.LastName = LastName;
                FirstName = "";
                OnPropertyChanged(nameof(FirstName));
                LastName = "";
                OnPropertyChanged(nameof(LastName));
                AddPersonModel.AddPersonToDB(_person);
            }
        }
        public bool CanAdd()
        {
            return (FirstName!=null && LastName!=null)&& (!FirstName.Equals("")&&!LastName.Equals(""));
        }

    }
}
