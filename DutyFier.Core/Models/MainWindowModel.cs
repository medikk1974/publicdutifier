﻿using DutyFier.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DutyFier.Core.Models
{
    public class MainWindowModel
    {
        private DutyRepository dutyRepository;

        public MainWindowModel(DutyRepository dutyRepository)
        {
            this.dutyRepository = dutyRepository;
        }

        public int GetUncreatedFeedbackCount()=>dutyRepository.GetAll().
                                                    Where(duty => !duty.IsApproved).
                                                    Where(duty=> duty.Date > DateTime.Now).
                                                    Count();

        public List<Duty> GetAll() => dutyRepository.GetAll().ToList();
    }

}
