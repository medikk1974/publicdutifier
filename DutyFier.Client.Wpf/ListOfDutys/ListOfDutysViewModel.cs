﻿using DutyFier.Core;
using DutyFier.Core.Entities;
using DutyFier.Core.Models;
using System.Collections.Generic;
using System.Windows;
using Unity;

namespace DutyFier.Client.Wpf.ListOfDutys
{
    public class ListOfDutysViewModel
    {
        private FeedbackModel FeedbackModel { get; set; }
        public List<Duty> Dutys { get; set; }
        public Visibility ContentVisibility
        {
            get { return Dutys.Count > 0 ? Visibility.Visible : Visibility.Hidden; }
        }
        public ListOfDutysViewModel(FeedbackModel feedbackModel)
        {
            FeedbackModel = feedbackModel;
            Dutys = FeedbackModel.GetDuties();
        }
    }
}
