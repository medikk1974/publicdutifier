﻿using System.Windows;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Generate
{
    /// <summary>
    /// Логика взаимодействия для PreView.xaml
    /// </summary>
    public partial class PreView : UserControl
    {
        public PreView(State.GenerateContext context)
        {
            InitializeComponent();
            DataContext = new PreViewModel(context);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dataGrid.Items.Refresh();
        }
    }
}
