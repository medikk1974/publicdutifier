﻿using DutyFier.Core.Entities;
using DutyFier.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DutyFier.Core.Models
{
    public class PersonModel
    {
        private IRepository<Person> PersonRepository { get; set; }
        public PersonModel(IRepository<Person> personRepository)
        {
            PersonRepository = personRepository;
        }
        public List<Person> GetAllPerson() => PersonRepository.GetAll().ToList();
        public void RemovePerson(Person person) => PersonRepository.Delete(person);
        public void UpdatePersonDependencyToPosition(Person selectedPerson)
        {
            PersonRepository.Update(selectedPerson);
        }
    }
}
