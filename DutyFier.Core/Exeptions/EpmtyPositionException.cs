﻿using System;

namespace DutyFier.Core.Exeptions
{
    class EpmtyPositionException : ArgumentNullException
    {
        public EpmtyPositionException(string message) : base(message)
        {
        }
    }
}
