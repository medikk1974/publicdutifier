﻿using DutyFier.Core.Entities;
using DutyFier.Core.Interfaces;

namespace DutyFier.Core.Models
{
    public class AdTypeModel
    {
        private IRepository<DutyType> DutyTypeRepository { get; set;}

        public AdTypeModel(IRepository<DutyType> dutyTypeRepository)
        {
            DutyTypeRepository = dutyTypeRepository;
        }

        public void AddDutyTypeToDB(DutyType dutyType)
        {
            DutyTypeRepository.Create(dutyType);
        }
    }
}
