﻿using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Statistics
{
    /// <summary>
    /// Логика взаимодействия для StatisticksView.xaml
    /// </summary>
    public partial class StatisticsView : UserControl
    {
        public StatisticsView(StatisticsViewModel statisticsViewModel)
        {
            InitializeComponent();
            DataContext = statisticsViewModel;
        }
    }
}
