﻿using DutyFier.Core.Models;
using System.Windows.Controls;

namespace DutyFier.Client.Wpf.ListOfDutys
{
    /// <summary>
    /// Логика взаимодействия для ListOfDutys.xaml
    /// </summary>
    public partial class ListOfDutysView : UserControl
    {
        public ListOfDutysView(ListOfDutysViewModel listOfDutys)
        {
            InitializeComponent();
            DataContext = listOfDutys;
        }
    }
}
