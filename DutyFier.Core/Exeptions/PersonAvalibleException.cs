﻿using System;

namespace DutyFier.Core.Exeptions
{
    public class PersonAvalibleException : ArgumentException
    {
        public PersonAvalibleException(string message) : base(message)
        {
        }
    }
}
