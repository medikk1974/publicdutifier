﻿using System.Windows.Controls;

namespace DutyFier.Client.Wpf.Settings
{
    /// <summary>
    /// Логика взаимодействия для SettingsView.xaml
    /// </summary>
    public partial class SettingsView : UserControl
    {
        private SettingsViewModel svw;

        public SettingsView(SettingsViewModel settingsViewModel)
        {
            svw = settingsViewModel;
            InitializeComponent();
            DataContext = svw;
        }
    }
}